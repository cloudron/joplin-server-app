**Joplin Server may be used for personal non-commercial purposes only.** Please carefully read and review [the license](https://github.com/laurent22/joplin/blob/dev/packages/server/LICENSE.md).

This app is pre-setup with an admin account. The initial credentials are:
  
**Email**: admin@localhost<br/>
**Password**: admin<br/>

Please change the admin password and email immediately.

Download the clients here https://joplinapp.org/download/.
