#!/bin/bash

set -eu

mkdir -p /run/joplin/log /run/joplin/tmp

export RUNNING_IN_DOCKER=1
export APP_PORT=3000
export APP_BASE_URL=${CLOUDRON_APP_ORIGIN}

export DB_CLIENT=pg
export POSTGRES_PASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD}
export POSTGRES_DATABASE=${CLOUDRON_POSTGRESQL_DATABASE}
export POSTGRES_USER=${CLOUDRON_POSTGRESQL_USERNAME}
export POSTGRES_PORT=${CLOUDRON_POSTGRESQL_PORT}
export POSTGRES_HOST=${CLOUDRON_POSTGRESQL_HOST}

export MAILER_ENABLED=1
export MAILER_HOST=${CLOUDRON_MAIL_SMTP_SERVER}
export MAILER_PORT=${CLOUDRON_MAIL_SMTP_PORT}
export MAILER_SECURITY=none
export MAILER_AUTH_USER=${CLOUDRON_MAIL_SMTP_USERNAME}
export MAILER_AUTH_PASSWORD=${CLOUDRON_MAIL_SMTP_PASSWORD}
export MAILER_NOREPLY_NAME=${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Joplin}
export MAILER_NOREPLY_EMAIL=${CLOUDRON_MAIL_FROM}

export ACCOUNT_TYPES_ENABLED=0

[[ ! -f /app/data/config ]] && cp /app/pkg/config.template /app/data/config

export $(cat /app/data/config |grep "^[^#]" | xargs)

chown -R cloudron:cloudron /run/joplin /app/data

echo "==> starting server"
cd /app/code
exec gosu cloudron:cloudron yarn --cwd packages/server start

