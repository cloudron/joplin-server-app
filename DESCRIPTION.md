## About

Joplin is a free, open source note taking and to-do application, which can handle a large number of notes organised into notebooks. The notes are searchable, can be copied, tagged and modified either from the applications directly or from your own text editor. The notes are in Markdown format.

## License

**JOPLIN SERVER PERSONAL USE LICENSE**

Please carefully read and review [the license](https://github.com/laurent22/joplin/blob/dev/packages/server/LICENSE.md).

In particular, Joplin Server may be used for personal non-commercial purposes only.

## Features

* Desktop, mobile and terminal applications.
* Web Clipper for Firefox and Chrome.
* End To End Encryption (E2EE).
* Note history (revisions).
* Synchronisation with various services, including Nextcloud, Dropbox, WebDAV and OneDrive.
* Offline first, so the entire data is always available on the device even without an internet connection.
* Import Enex files (Evernote export format) and Markdown files.
* Export JEX files (Joplin Export format) and raw files.
* Support notes, to-dos, tags and notebooks.
* Sort notes by multiple criteria - title, updated time, etc.
* Support for alarms (notifications) in mobile and desktop applications.
* Markdown notes, which are rendered with images and formatting in the desktop and mobile applications. Support for extra features such as math notation and checkboxes.
* Choice of both Markdown and Rich Text (WYSIWYG) editors.
* File attachment support - images are displayed, other files are linked and can be opened in the relevant application.
* Inline display of PDF, video and audio files.
* Goto Anything feature.
* Search functionality.
* Geo-location support.
* Supports multiple languages.
* External editor support - open notes in your favorite external editor with one click in Joplin.
* Extensible functionality through plugin and data APIs.
* Template support with data variables for auto creation of time & dates.
* Custom CSS support for customisation of both the rendered markdown and overall user interface.
* Customisable layout allows toggling, movement and sizing of various elements.
* Keyboard shortcuts are editable and allow binding of most Joplin commands with export/import functionality.

