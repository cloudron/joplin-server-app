[0.1.0]
- Initial commit

[0.2.0]
- Update to server 2.2.9 beta

[0.3.0]
- Update to server 2.2.10

[0.4.0]
- Update to server 2.2.12

[1.0.0]
- Update Joplin Server to 2.5.1

[1.0.1]
- Update Joplin Server to 2.5.2

[1.1.0]
* Update Joplin Server to 2.5.5

[1.1.1]
* Update Joplin Server to 2.5.6

[1.1.2]
* Update Joplin Server to 2.5.7

[1.1.3]
* Update Joplin Server to 2.5.9

[1.1.4]
* Update Joplin Server to 2.5.10

[1.2.0]
* Update Joplin Server to 2.6.2

[1.2.1]
* Update Joplin Server to 2.6.3

[1.2.2]
* Update Joplin Server to 2.6.5

[1.2.3]
* Update Joplin Server to 2.6.8

[1.2.4]
* Update Joplin Server to 2.6.9

[1.2.5]
* Update Joplin Server to 2.6.10

[1.2.6]
* Update Joplin Server to 2.6.11

[1.2.7]
* Update Joplin Server to 2.6.12

[1.2.8]
* Update Joplin Server to 2.6.13

[1.2.9]
* Update Joplin Server to 2.6.14

[1.3.0]
* Update Joplin Server to 2.7.3

[1.3.1]
* Update Joplin Server to 2.7.4

[1.4.0]
* Fix email delivery

[1.5.0]
* Update Joplin Server to 2.9.1

[1.5.1]
* Update Joplin Server to 2.9.2

[1.5.2]
* Update Joplin Server to 2.9.6

[1.6.0]
* Update Joplin Server to 2.9.7
* Email display name support

[1.7.0]
* Update Joplin Server to 2.10.3
* Update Cloudron base image to 4.0.0

[1.7.1]
* Update Joplin Server to 2.10.4

[1.7.2]
* Update Joplin Server to 2.10.5

[1.7.3]
* Update Joplin Server to 2.10.6

[1.7.4]
* Update Joplin Server to 2.10.8

[1.7.5]
* Update Joplin Server to 2.10.9

[1.7.6]
* Update Joplin Server to 2.10.10

[1.7.7]
* Update Joplin Server to 2.10.11

[1.8.0]
* Update Joplin Server to 2.11.1

[1.8.1]
* Update Joplin Server to 2.11.2

[1.9.0]
* Update Joplin Server to 2.12.1

[1.10.0]
* Update Joplin Server to 2.13.1

[1.11.0]
* Update base iamge to 4.2.0

[1.11.1]
* Update Joplin Server to 2.13.2

[1.11.2]
* Update Joplin Server to 2.13.3

[1.11.3]
* Update Joplin Server to 2.13.4

[1.11.4]
* Update Joplin Server to 2.13.5

[1.12.0]
* Update Joplin Server to 2.14.1

[1.12.1]
* Update Joplin Server to 2.14.2

[2.0.0]
* Update Joplin Server to 3.0.1

[2.1.0]
* Update joplin to 3.3.2
* [Full Changelog](https://github.com/laurent22/joplin/blob/dev/readme/about/changelog/server.md#server-v332---2025-02-19t220004z)
* Improved: Accessibility: Make click outside of dialog content be cancellable  ([#&#8203;11765](https://github.com/laurent22/joplin/issues/11765) by [@&#8203;pedr](https://github.com/pedr))
* Improved: Improve behaviour of note list to-dos when ticking a checkbox using the keyboard ([`44c735a`](https://github.com/laurent22/joplin/commit/44c735a))
* Improved: Improve usability of note list when ticking to-dos using the Space key ([#&#8203;11855](https://github.com/laurent22/joplin/issues/11855))
* Improved: Plugins: Simplify getting the ID of the note open in an editor ([#&#8203;11841](https://github.com/laurent22/joplin/issues/11841) by [@&#8203;personalizedrefrigerator](https://github.com/personalizedrefrigerator))
* Fixed: Fix OneNote importer not being able to handle corrupted attachments ([#&#8203;11859](https://github.com/laurent22/joplin/issues/11859)) ([#&#8203;11844](https://github.com/laurent22/joplin/issues/11844) by [@&#8203;pedr](https://github.com/pedr))
* Fixed: Fix Rich Text right-click and paste regressions ([#&#8203;11850](https://github.com/laurent22/joplin/issues/11850) by [@&#8203;personalizedrefrigerator](https://github.com/personalizedrefrigerator))
* Fixed: Hide extra clear button in search field ([#&#8203;11851](https://github.com/laurent22/joplin/issues/11851)) ([#&#8203;11847](https://github.com/laurent22/joplin/issues/11847) by [@&#8203;personalizedrefrigerator](https://github.com/personalizedrefrigerator))
* Fixed: Preserve attachment file extensions regardless of the mime type  ([#&#8203;11852](https://github.com/laurent22/joplin/issues/11852)) ([#&#8203;11759](https://github.com/laurent22/joplin/issues/11759) by [@&#8203;pedr](https://github.com/pedr))

[2.1.1]
* Update joplin to 3.3.3

[2.1.2]
* Update joplin to 3.3.4

[2.2.0]
* Base image 5.0.0

