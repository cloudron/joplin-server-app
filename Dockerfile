FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code
WORKDIR /app/code

RUN apt-get update && \
    apt-get install -y libsecret-1-0 && \
    rm -r /var/cache/apt /var/lib/apt/lists

# https://github.com/laurent22/joplin/blob/dev/Dockerfile.server#L5
ARG NODE_VERSION=18.20.7
RUN mkdir -p /usr/local/node-$NODE_VERSION && \
    curl -L https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-$NODE_VERSION
ENV PATH /usr/local/node-$NODE_VERSION/bin:$PATH

# renovate: datasource=github-tags depName=laurent22/joplin versioning=semver extractVersion=^server-v(?<version>.+)$
ARG JOPLIN_SERVER_VERSION=3.3.4

# https://github.com/laurent22/joplin/blob/dev/Dockerfile.server
RUN curl -L https://github.com/laurent22/joplin/archive/refs/tags/server-v${JOPLIN_SERVER_VERSION}.tar.gz | tar zx --strip-components 1 -C /app/code && \
    rm -R /app/code/packages/app-cli && \
    rm -R /app/code/packages/app-clipper && \
    rm -R /app/code/packages/app-desktop && \
    rm -R /app/code/packages/app-mobile && \
    rm -R /app/code/packages/generator-joplin && \
    rm -R /app/code/packages/plugin-repo-cli && \
    rm -R /app/code/packages/plugins

# https://github.com/laurent22/joplin/blob/4ed7c340a04131f351676cf5f581ced8428e7d2f/Dockerfile.server#L79
RUN npm --no-update-notifier install -g yarn && \
    yarn install --inline-builds && \
    yarn cache clean

RUN rm -rf /app/code/packages/server/logs && ln -s /run/joplin/log /app/code/packages/server/logs
RUN rm -rf /app/code/packages/server/temp && ln -s /run/joplin/tmp /app/code/packages/server/temp

COPY config.template start.sh /app/pkg/

CMD [ "/app/pkg/start.sh"]
